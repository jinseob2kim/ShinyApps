# Health exam summary

## Run on server

http://www.jinseob2kim.com/shiny/jinseob2kim/health-report/


## Run on rstudio

```r
library(shiny)
runGitHub("ShinyApps", "jinseob2kim", subdir = "health-report")  
```

