# ShinyApps

[![GitHub issues](https://img.shields.io/github/issues/jinseob2kim/ShinyApps.svg)](https://github.com/jinseob2kim/ShinyApps/issues)
[![GitHub forks](https://img.shields.io/github/forks/jinseob2kim/ShinyApps.svg)](https://github.com/jinseob2kim/ShinyApps/network)
[![GitHub stars](https://img.shields.io/github/stars/jinseob2kim/ShinyApps.svg)](https://github.com/jinseob2kim/ShinyApps/stargazers)
[![GitHub license](https://img.shields.io/github/license/jinseob2kim/ShinyApps.svg)](https://github.com/jinseob2kim/ShinyApps/blob/master/LICENSE)
[![GitHub last commit](https://img.shields.io/github/last-commit/google/skia.svg)](https://github.com/jinseob2kim/ShinyApps)

Example 

```r
library(shiny)
runGitHub("ShinyApps", "jinseob2kim", subdir = "gwas")  
```


