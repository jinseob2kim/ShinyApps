TableOneUI <- function(id, factor_vars, vars) {
  ns <- NS(id)
  sidebarLayout(
    sidebarPanel(
      selectizeInput(ns("strata"), "Stratified by", 
                     choices = factor_vars[1], multiple = F, 
                     selected = factor_vars
      ),
      selectizeInput(ns("selected_vars"), "Variables", 
                     choices = selected_vars, multiple = T, 
                     selected = selected_vars
      ),
      checkboxInput(ns("smd"), "Show SMD", F)
    ),
    mainPanel(
      DT::dataTableOutput(ns("table1"))
    )
  )
  
}




library(tableone);library(jstable)
TableOne <- function(input, output, session, data, factor_vars) {
  
  res= CreateTableOne(vars =input$vars, strata = input$strata, data = data, factorVars = factor_vars) 
  ptb1= print(res, 
              showAllLevels=T,
              printToggle=F, quote=F, smd = input$smd, varLabels = F)
  
  rownames(ptb1) = gsub("(mean (sd))", "", rownames(ptb1), fixed=T)
  ptb1 = ptb1[,-which(colnames(ptb1) == "test")]
  sig = ifelse(ptb1[,"p"] == "<0.001", 0, as.numeric(as.vector(ptb1[,"p"])))
  sig = ifelse(sig <= 0.05, "**","")
  cap.tb1 = paste("Table 1: Stratified by ",input$strata, sep="")
  ptb1 = cbind(ptb1, sig)
  
  datatable(ptb1, rownames=T, extension= "Buttons", caption = cap.tb1,
            options = c(opt.tb1("tb1"), 
                        list(columnDefs = list(list(visible=FALSE, targets=ncol(ptb1)))
                        ),
                        list(scrollX = TRUE)
            )
  ) %>% formatStyle("sig", target = 'row',backgroundColor = styleEqual("**", 'yellow'))
}
