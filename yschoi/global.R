## Packages load
library(shiny);library(data.table);library(DT);library(markdown);library(tableone);library(glue);library(epiDisplay);library(survival);library(ggplot2)

## For ggkm
#devtools::install_github("michaelway/ggkm")
library(ggkm)

## For svg
library(svglite)

## For var label
library(labelled)

## For js
library(jstable)

## Data
a = fread("https://s3.ap-northeast-2.amazonaws.com/consultdata/yschoi/ER+and+EROP+2018.07.01+%EC%A0%84%EC%B2%B4+%ED%99%98%EC%9E%90%EA%B5%B0.csv")
names(a) = gsub("\n","", names(a))
names(a) = gsub(" ", "_", names(a))
names(a)[17:18] = c("D_RM", "L_RM")
a[, `Endo_Yr` := as.numeric(tstrsplit(`Endo_Tx`, "/")[[3]])]
a[, `Endo _Yr_cat` := ifelse(`Endo_Yr` <=2005, "1996-2005", ifelse(`Endo_Yr` <=2010, "2006-2010", "2011-2015"))]
factor_vars = names(a)[c(5, 42, 10:13, 15:22, 33 )]
a[, (factor_vars) := lapply(.SD, as.factor), .SDcols= factor_vars]
date_var = c("Birth", "insurance", "Endo_Tx", "OP_date", "CT_date", "Death_date")
b = a[, .SD, .SDcols = -date_var]



## Label data 
mk.lev.var = function(data = a, vname ="Sex"){
  v.vec = a[[vname]]
  out = ""
  if (is.numeric(v.vec)){
    out = c(vname, class(v.vec), NA)
  } else{
    v.level = levels(v.vec)
    nr = length(v.level)
    out = cbind(rep(vname, nr), rep(class(v.vec), nr), v.level)
  }
  return(out)
}

mk.lev = function(data = a){
  out.list = lapply(names(data), function(x){mk.lev.var(data, x)})
  out.dt = data.table(Reduce(rbind, out.list))
  names(out.dt) = c("variable", "class","level")
  out.dt[, var_label := variable]
  out.dt[, val_label := level]
  return(out.dt[])
}

b.label = mk.lev(b)  ### label data



## Input label
b.label[variable == "sex", var_label := "Sex"]
b.label[variable == "sex", val_label := c("Male", "Female")]
b.label[variable == "Endo Yr cat", var_label := "Period"]
b.label[variable == "Hospital", val_label := c("AMC", "Other")]
b.label[variable == "Location2", var_label := "Location"]
b.label[variable == "Location2", val_label := c("Cecum", "Ascending colon", "Transverse colon", "Descending colon", "Sigmoid colon", "Rectum")]
b.label[variable == "modality", var_label := "Modality"]
b.label[variable == "modality", val_label := c("EMR", "ESD")]
b.label[variable == "piecemeal", var_label := "Piecemeal"]
b.label[variable == "piecemeal", val_label := c("En bloc", "Piecemeal")]
b.label[variable == "Depth", val_label := c("Sm1", "Sm, Unknown" ,"Sm2/Sm3")]
b.label[variable == "Diff", `:=`(var_label = "Differentiation", val_label = c("Good", "Moderate", "Poor", "Other"))]
b.label[variable == "D_RM", `:=`(var_label = "Deep resection margin", val_label = c("Negative", "Adenoma", "Cancer", "Other"))]
b.label[variable == "L_RM", `:=`(var_label = "Lateral resection margin", val_label = c("Negative", "Adenoma", "Cancer", "Other"))]
b.label[variable == "Lvi", `:=`(var_label = "Lymphovascular invasion", val_label = c("Negative", "Positive", "Unknown"))]
b.label[variable == "PNi", `:=`(var_label = "Perineural invasion", val_label = c("Negative", "Positive", "Unknown"))]
b.label[variable == "factor", `:=`(var_label = "High risk pathology", val_label = c("0", "1 or more"))]
b.label[variable == "Op_specimen", `:=`(var_label = "OP residual tumor", val_label = c("No", "Yes"))]


##ggkm sub
data.ggkm2 = b[factor ==1 & !is.na(Op_specimen)]









lmTable = function(epiDisplay.obj){
  tb.main = epiDisplay.obj$table
  tb.compact = tb.main[!rownames(tb.main)=="", ]
  if (sum(grepl("crude", colnames(tb.main))) ==0){
    tb.compact = tb.main
  }
  ## Var label
  tb.col = gsub(" \\(cont. var.\\)", "", rownames(tb.compact))
  ll = strsplit(epiDisplay.obj$last.lines,"\n")[[1]]
  ll.vec = matrix(unlist(lapply(ll,function(x){strsplit(x," = ")})), ncol =2, byrow=T)
  ll.mat = matrix(rep("", nrow(ll.vec)* ncol(tb.compact)), nrow = nrow(ll.vec))  
  ll.mat[,1] = ll.vec[,2]
  rownames(ll.mat) = ll.vec[,1]
  out = rbind(tb.compact, rep("", ncol(tb.compact)), ll.mat)
  if (sum(grepl("crude", colnames(tb.main))) ==0){
    out = rbind(tb.compact, ll.mat)
  }
  p.colnum = which(colnames(out) %in% c("P(t-test)", "P(Wald's test)")) 
  
  pn = gsub("< ","", out[, p.colnum])
  out.df = data.frame(out)
  colnames(out.df) = colnames(out)
  colnames(out.df)[p.colnum] = "adj. P value"
  out.df$sig = as.numeric(pn)
  out.df$sig = ifelse(out.df$sig <= 0.05, "**", "")
  return(out.df)
}

lmLabelOne = function(xbar, ref = a.label){
  xbar.label = ref[variable == xbar]
  xbar.class = xbar.label[, class][1]
  xbar.lev = xbar.label[, val_label]
  if (xbar.class %in% c("integer", "integer64", "numeric", "double")){
    return(xbar.label[, var_label])
  } else if (nrow(xbar.label) == 2){
    return(paste(xbar.label[, var_label][1],": ", xbar.lev[2], " vs ", xbar.lev[1], sep=""))
  } else {
    lab.ref = paste(xbar.label[, var_label][1], ": ref.=", xbar.lev[1], sep="")
    return(c(lab.ref, paste("   ", xbar.lev[-1], sep="")))
  }
  
}

LabelTable = function(epiDisplay.obj, xbars, ref){
  lmtb = lmTable(epiDisplay.obj)
  sp = which(rownames(lmtb) == "")
  rownames(lmtb)[1:(sp-1)] = unlist(sapply(xbars, lmLabelOne, ref))
  return(lmtb)
}

LabelTable2 = function(fulltb.obj, xbars, ref){
  sp = which(rownames(fulltb.obj) == "")
  rownames(fulltb.obj)[1:(sp-1)] = unlist(sapply(xbars, lmLabelOne, ref))
  return(fulltb.obj)
}

LabelTable3 = function(lmerMod.obj.tb, xbars, ref, id){
  rn = nrow(lmerMod.obj.tb)
  sp = which(rownames(lmerMod.obj.tb) == "Random effects")
  rownames(lmerMod.obj.tb)[1:(sp-1)] = unlist(sapply(xbars, lmLabelOne, ref))
  sp2 = which(rownames(lmerMod.obj.tb) == "Metrics")
  rownames(lmerMod.obj.tb)[(sp+1): (sp2-1)] = gsub(id, ref[variable == id, var_label][1], rownames(lmerMod.obj.tb)[(sp+1): (sp2-1)])
  rownames(lmerMod.obj.tb)[(sp2+1): rn] = gsub(id, ref[variable == id, var_label][1], rownames(lmerMod.obj.tb)[(sp2+1): rn])
  return(lmerMod.obj.tb)
  
}

